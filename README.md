---

**WARNING!**: This is the *Old* source-code repository for Math Extra shared Package from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/tcellxtalk/math_extras_code/) located at https://sourceforge.net/p/lp-csic-uab/tcellxtalk/math_extras_code/**  

---  
  
  
# Math Extra shared Package

Extra math functions and tools for Python 2.7.x projects  


---

**WARNING!**: This is the *Old* source-code repository for Math Extra shared Package from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/tcellxtalk/math_extras_code/) located at https://sourceforge.net/p/lp-csic-uab/tcellxtalk/math_extras_code/**  

---  
  