# -*- coding: utf-8 -*-

from __future__ import division

"""
:synopsis: Common Statistic Functions for Python.

:created:    2014/10/16

:authors:    Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014-2016 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.2'
__UPDATED__ = '2016-07-21'

#===============================================================================
# Imports
#===============================================================================
import numpy as np
from scipy import histogram
from scipy.optimize import curve_fit
from scipy import stats
from matplotlib_venn import venn2, venn3
from matplotlib.pyplot import figure, title, savefig

from pystats import *

#===============================================================================
# Functions declarations
#===============================================================================
# Functions gauss() and calc_normal() copied and adapted from LymPHOS
# PQuantifier pq_analyze.py at revision 123:
def gauss(x, A, mu, sigma):
    """
    Returns f(x) for a gaussian function.
    X-NOTE: https://en.wikipedia.org/wiki/Normal_distribution
    In the canonical equation A = 1 / (sigma * (2 * pi)**0.5)
    """
    x = np.array(x)
    return A * np.exp( -(x - mu)**2 / (2 * sigma**2) )
#
#
def calc_normal(x, limits=(-10,10)):
    """
    Calculates mu, sigma from a gaussian fit of the x distribution.
    Curve fitting produces wrong results for distributions with few values:
    bins with 0 frequency affects calculation. This must be addressed.
    """
    nbins = 600
    xmin, xmax = limits
    bins = np.linspace(xmin, xmax, nbins)

    n, _ = histogram(x, bins)
    diff = ( bins[1] - bins[0] ) / 2
    bin_centers = [ bin + diff for bin in bins[:-1] ]
    A, mu, sigma = curve_fit(gauss, bin_centers, n)[0]

    #gauss curve_fit sometimes produces negative sigmas
    sigma = abs(sigma)

    return A, mu, sigma, limits, nbins


def tstudent(alpha, df):
    """
    Return T values from a real t-student distribution, not a table of pre-
    calculated values
    """
    T = stats.t.interval(1-alpha, df)[1]
    return T


def plot_venn(groups, labels=('A', 'B', 'C'), title_=None, filename_root=None,
              file_type='svg'):
    """
    Compare 2 or 3 sets (in groups), and save the resulting venn diagram to disk
    if filename_root is supplied.

    Returns the compared data sets (data), their sizes (comp_set_sizes), and the
    venn diagram (venn)

    Derived from Quim function in tools
    """
    if len(groups) == 3:
        a, b, c = groups
        data = (a.difference(b, c),
                b.difference(a, c),
                a.intersection(b).difference(c),
                c.difference(b, a),
                a.intersection(c).difference(b),
                b.intersection(c).difference(a),
                a.intersection(b, c)
                )
        venn_func = venn3
    elif len(groups) == 2:
        a, b = groups
        data = (a.difference(b),
                b.difference(a),
                a.intersection(b)
                )
        venn_func = venn2
    else:
        raise ValueError("Only can draw Venn diagrams of 2 or 3 sets")

    f = figure() #New matplotlib.pyplot context
    comp_set_sizes = map(len, data)
    venn = venn_func(subsets=comp_set_sizes, set_labels=labels)
    if title_:
        title(title_, fontsize=16)
    if filename_root:
        savefig( '{0}_{1}.{2}'.format(filename_root, '-'.join(labels), file_type),
                  dpi=600 )

    return data, venn, comp_set_sizes

