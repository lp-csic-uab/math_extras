# -*- coding: utf-8 -*-

from __future__ import division

"""
:synopsis: Common Statistic Functions for Python in standard Python (no external libraries).

:created:    2015/03/10

:authors:    Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014-2016 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.2'
__UPDATED__ = '2016-07-21'

#===============================================================================
# Imports
#===============================================================================
import math


#===============================================================================
# Functions declarations
#===============================================================================
def mean(values):
    """
    :return: mean value of ``values`` iterable.
    """
    return sum(values) / len(values)


def stdv(values):
    """
    :return: standard deviation of ``values`` iterable.
    """
    mean = mean(values)
    variance = sum([ (value - mean)**2 for value in values ]) / ( len(values) - 1 )
    return math.sqrt(variance)


def median(values):
    """
    :return: median value of ``values`` iterable.
    """
    working_val = sorted(values)
    #
    n = len(working_val)
    middle = int(n / 2)
    if n % 2 == 0: #Pair
        return ( working_val[middle - 1] + working_val[middle] ) / 2
    else: #Odd
        return working_val[middle]
    

def mix_normals(exp_normals, exp_pack=None):
    """
    X-NOTE: https://en.wikipedia.org/wiki/Mixture_density#Moments
    """
    if exp_pack:
        exps = exp_pack
    else:
        exps = exp_normals.keys()
    
    mixed_normal = exp_normals[exps[0]]
    for exp in exps[1:]:
        n = mixed_normal['n'] + exp_normals[exp]['n']
        summed_p = mixed_normal['n']/n
        otther_p = exp_normals[exp]['n']/n
        mu = (summed_p * mixed_normal['mu'] + 
              otther_p * exp_normals[exp]['mu'])
        sigma = math.sqrt(summed_p * ( (mixed_normal['mu'] - mu)**2 + 
                                       mixed_normal['sigma']**2) + 
                          otther_p * ( (exp_normals[exp]['mu'] - mu)**2 + 
                                       exp_normals[exp]['sigma']**2)
                          )
        mixed_normal = {'mu': mu, 'sigma': sigma, 'n': n}
    return mixed_normal


def ttest(mean1, mean2, sd1, sd2, n1, n2):
    """
    This test also known as Welch's t-test is used only when the two population
    variances are assumed to be different (the two sample sizes may or may not
    be equal) and hence must be estimated separately.
    
    - http://en.wikipedia.org/wiki/Student%27s_t-test#Unequal_sample_sizes.2C_unequal_variance
    - Welch, B. L. (1947), "The generalization of "student's" problem when 
      several different population variances are involved.", Biometrika 34: 28-35
    """
    mean_dif_variance = (sd1 ** 2) / n1 + (sd2 ** 2) / n2
    mean_dif_sd = math.sqrt(mean_dif_variance)
    t = (mean1 - mean2) / mean_dif_sd
    return abs(t)


def degrees_freedom(mean1, mean2, sd1, sd2, n1, n2):
    """
    This is the Welch-Satterthwaite equation.
    
    - Satterthwaite, F. E. (1946), "An Approximate Distribution of Estimates of 
      Variance Components.", Biometrics Bulletin 2: 110-114, doi:10.2307/3002019
    """
    sd1n1 = (sd1 ** 2) / n1
    sd2n2 = (sd2 ** 2) / n2
    df = ((sd1n1 + sd2n2) ** 2) / ((sd1n1 ** 2) / (n1 - 1) + 
                                   (sd2n2 ** 2) / (n2 - 1))
    return df

